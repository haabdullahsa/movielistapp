package com.tmdbapp.thelatestmovies.provider.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public class MovieListResponse implements Parcelable {

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<Movie> movies;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("total_results")
    private int totalResults;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    protected MovieListResponse(Parcel in) {
        page = in.readInt();
        movies = in.createTypedArrayList(Movie.CREATOR);
        totalPages = in.readInt();
        totalResults = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(page);
        dest.writeTypedList(movies);
        dest.writeInt(totalPages);
        dest.writeInt(totalResults);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieListResponse> CREATOR = new Creator<MovieListResponse>() {
        @Override
        public MovieListResponse createFromParcel(Parcel in) {
            return new MovieListResponse(in);
        }

        @Override
        public MovieListResponse[] newArray(int size) {
            return new MovieListResponse[size];
        }
    };
}
