package com.tmdbapp.thelatestmovies.provider.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.tmdbapp.thelatestmovies.provider.network.interceptor.AuthenticateInterceptor;
import com.tmdbapp.thelatestmovies.provider.network.service.MovieService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@Module
public class NetworkModule {

    private String endpointUrl;

    public NetworkModule(String endpointUrl) {
        this.endpointUrl = endpointUrl;
    }

    @Provides
    Gson provideGson() {
        return new GsonBuilder().setLenient().create();
    }

    @Provides
    AuthenticateInterceptor provideAuthenticateInterceptor() {
        return new AuthenticateInterceptor();
    }

    @Provides
    okhttp3.OkHttpClient provideOkHttpClient(AuthenticateInterceptor authenticateInterceptor) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
        okHttpClient.addInterceptor(authenticateInterceptor);
        return okHttpClient.build();
    }

    @Provides
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(endpointUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    MovieService provideMovieService(Retrofit retrofit) {
        return retrofit.create(MovieService.class);
    }
}
