package com.tmdbapp.thelatestmovies.provider.scope;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
