package com.tmdbapp.thelatestmovies.provider.interactor;

import com.tmdbapp.thelatestmovies.provider.constant.SortBy;
import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.provider.model.MovieListResponse;
import com.tmdbapp.thelatestmovies.provider.network.service.MovieService;
import com.tmdbapp.thelatestmovies.provider.scope.ApplicationScope;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@ApplicationScope
public class MovieInteractor {

    private MovieService movieService;

    @Inject
    MovieInteractor(MovieService movieService) {
        this.movieService = movieService;
    }

    public Observable<MovieListResponse> queryTheLatestMovieList(String releaseDate, int page) {
        return movieService.getMovies(releaseDate, SortBy.LATEST_MOVIES, page);
    }

    public Observable<Movie> queryMovieDetailById(int movieId) {
        return movieService.getMovie(movieId);
    }
}
