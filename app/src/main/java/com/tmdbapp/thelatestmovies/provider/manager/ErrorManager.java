package com.tmdbapp.thelatestmovies.provider.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tmdbapp.thelatestmovies.provider.model.ErrorResponse;
import com.tmdbapp.thelatestmovies.view.base.BaseView;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public abstract class ErrorManager<T> extends DisposableObserver<T> {

    private WeakReference<BaseView> weakReference;

    protected ErrorManager(BaseView view) {
        this.weakReference = new WeakReference<>(view);
    }

    public abstract void onSuccess(T t);

    @Override
    public void onNext(T t) {
        onSuccess(t);
    }

    @Override
    public void onError(Throwable e) {
        BaseView view = weakReference.get();
        view.hideDialog();
        if (e instanceof HttpException) {
            ResponseBody responseBody = ((HttpException) e).response().errorBody();
            Gson gson = new GsonBuilder().setLenient().create();
            ErrorResponse errorResponse = gson.fromJson(String.valueOf(responseBody), ErrorResponse.class);
            view.onShowError(errorResponse.getStatusMessage());
        } else {
            view.onShowError(e.getMessage());
        }
    }

    @Override
    public void onComplete() {

    }
}
