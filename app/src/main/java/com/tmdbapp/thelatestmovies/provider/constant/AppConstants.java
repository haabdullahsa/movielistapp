package com.tmdbapp.thelatestmovies.provider.constant;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.tmdbapp.thelatestmovies.provider.constant.AppConstants.API_KEY;
import static com.tmdbapp.thelatestmovies.provider.constant.AppConstants.ENDPOINT_URL;
import static com.tmdbapp.thelatestmovies.provider.constant.AppConstants.IMAGE_URL;


@StringDef({API_KEY, ENDPOINT_URL, IMAGE_URL})
@Retention(RetentionPolicy.RUNTIME)
public @interface AppConstants {
    String API_KEY = "38db3300fdb919869cf093152485a4ac";
    String ENDPOINT_URL = "http://api.themoviedb.org/3/";
    String IMAGE_URL = "https://image.tmdb.org/t/p/w500";
    String ARG_CALLBACK_TO_ACTIVITY = "ARG_CALLBACK";
    String ARG_MOVIE_ID = "MOVIE_ID";
}
