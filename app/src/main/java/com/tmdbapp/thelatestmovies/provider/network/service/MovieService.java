package com.tmdbapp.thelatestmovies.provider.network.service;

import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.provider.model.MovieListResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public interface MovieService {
    @GET("discover/movie")
    Observable<MovieListResponse> getMovies(@Query("primary_release_date.lte") String releaseDate,
                                            @Query("sort_by") String sortBy, @Query("page") int page);

    @GET("movie/{id}")
    Observable<Movie> getMovie(@Path("id") int id);
}
