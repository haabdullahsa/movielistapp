package com.tmdbapp.thelatestmovies.provider.constant;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.tmdbapp.thelatestmovies.provider.constant.SortBy.LATEST_MOVIES;
import static com.tmdbapp.thelatestmovies.provider.constant.SortBy.OLDEST_MOVIES;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@StringDef({OLDEST_MOVIES, LATEST_MOVIES})
@Retention(RetentionPolicy.RUNTIME)
public @interface SortBy {
    String OLDEST_MOVIES = "release_date.asc";
    String LATEST_MOVIES = "release_date.desc";
}
