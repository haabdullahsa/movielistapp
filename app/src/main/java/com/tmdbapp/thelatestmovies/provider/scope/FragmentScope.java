package com.tmdbapp.thelatestmovies.provider.scope;

import javax.inject.Scope;

/**
 * Created by abdullahsa on 14.11.2017.
 */
@Scope
public @interface FragmentScope {
}
