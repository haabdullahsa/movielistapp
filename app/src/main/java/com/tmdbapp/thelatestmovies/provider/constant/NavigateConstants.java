package com.tmdbapp.thelatestmovies.provider.constant;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants.DEFAULT_MOVIE_ID;
import static com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants.DETAİL_PAGE_NUM;
import static com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants.MOVIE_LIST_PAGE_NUM;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@IntDef({MOVIE_LIST_PAGE_NUM, DETAİL_PAGE_NUM, DEFAULT_MOVIE_ID})
@Retention(RetentionPolicy.RUNTIME)
public @interface NavigateConstants {
    int MOVIE_LIST_PAGE_NUM = 0;
    int DETAİL_PAGE_NUM = 1;
    int DEFAULT_MOVIE_ID = -1;
}
