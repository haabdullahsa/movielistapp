package com.tmdbapp.thelatestmovies.provider.network;

import com.tmdbapp.thelatestmovies.core.AppModule;
import com.tmdbapp.thelatestmovies.provider.interactor.MovieInteractor;
import com.tmdbapp.thelatestmovies.provider.scope.ApplicationScope;

import dagger.Component;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@ApplicationScope
@Component(modules = {AppModule.class, NetworkModule.class})
public interface NetworkComponent {

    //Interactors
    MovieInteractor movieInteractor();
}
