package com.tmdbapp.thelatestmovies.core;

import android.app.Application;

import com.tmdbapp.thelatestmovies.provider.constant.AppConstants;
import com.tmdbapp.thelatestmovies.provider.network.DaggerNetworkComponent;
import com.tmdbapp.thelatestmovies.provider.network.NetworkComponent;
import com.tmdbapp.thelatestmovies.provider.network.NetworkModule;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public class TheLatestMoviesApp extends Application {

    private NetworkComponent networkComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        networkComponent = DaggerNetworkComponent.builder().appModule(new AppModule(this))
                .networkModule(new NetworkModule(AppConstants.ENDPOINT_URL))
                .build();
    }

    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }

}
