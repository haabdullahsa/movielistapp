package com.tmdbapp.thelatestmovies.core;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abdullahsa on 14.11.2017.
 */
@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

}
