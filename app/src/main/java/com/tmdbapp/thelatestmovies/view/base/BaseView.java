package com.tmdbapp.thelatestmovies.view.base;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public interface BaseView {
    void onShowError(String message);

    void showDialog();

    void hideDialog();
}
