package com.tmdbapp.thelatestmovies.view.base;

import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public class BasePresenterImpl<BaseViewType extends BaseView>
        implements BasePresenter<BaseViewType> {

    public BaseViewType baseView;
    public CompositeDisposable compositeDisposable;
    Unbinder unbinder;

    @Override
    public void onAttachView(BaseViewType baseView, Unbinder unbinder) {
        this.baseView = baseView;
        this.compositeDisposable = new CompositeDisposable();
        this.unbinder = unbinder;
    }

    @Override
    public void onDetachView() {
        this.baseView = null;
        this.compositeDisposable.dispose();
        this.compositeDisposable = null;
        unbinder.unbind();
        this.unbinder = null;
    }

}
