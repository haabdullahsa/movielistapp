package com.tmdbapp.thelatestmovies.view.child.detail;

import com.tmdbapp.thelatestmovies.provider.interactor.MovieInteractor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@Module
public class MovieDetailModule {

    @Provides
    MovieDetailContract.MovieDetailPresenter provideMoviePresenter(MovieInteractor movieInteractor) {
        return new MovieDetailPresenterImpl(movieInteractor);
    }
}
