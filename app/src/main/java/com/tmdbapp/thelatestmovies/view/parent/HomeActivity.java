package com.tmdbapp.thelatestmovies.view.parent;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.tmdbapp.thelatestmovies.R;
import com.tmdbapp.thelatestmovies.provider.constant.AppConstants;
import com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants;
import com.tmdbapp.thelatestmovies.view.child.detail.MovieDetailFragment;
import com.tmdbapp.thelatestmovies.view.child.list.MovieListFragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public class HomeActivity extends AppCompatActivity implements HomeContract.HomeView {

    private Unbinder unbinder;

    private String movieListFragmentTag;
    private String movieDetailFragmentTag;

    private HomePresenterImpl homePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        homePresenter = new HomePresenterImpl();
        unbinder = ButterKnife.bind(this);
        homePresenter.onAttachView(this, unbinder);
        homePresenter.navigateToPage(NavigateConstants.MOVIE_LIST_PAGE_NUM, NavigateConstants.DEFAULT_MOVIE_ID);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homePresenter.onDetachView();
    }

    @Override
    public void onShowMovieList() {
        final Fragment fragment = new MovieListFragment();
        movieListFragmentTag = addOrBringFragment(movieListFragmentTag, fragment, NavigateConstants.DEFAULT_MOVIE_ID);
    }

    @Override
    public void onShowMovieDetail(int movieId) {
        final Fragment fragment = new MovieDetailFragment();
        movieDetailFragmentTag = addOrBringFragment(movieDetailFragmentTag, fragment, movieId);
    }

    private String addOrBringFragment(String tag, Fragment fragment, int movieId) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (TextUtils.isEmpty(tag)) {
            tag = getFragmentTag(fragment);
            setExtras(fragment, movieId);
            fragmentTransaction.replace(R.id.frame_container, fragment, tag);
        } else {
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(tag);
            setExtras(fragment, movieId);
            fragmentTransaction.show(currentFragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
        return tag;
    }

    private void setExtras(Fragment fragment, int movieId) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.ARG_CALLBACK_TO_ACTIVITY, homeCallbackListener);
        if (movieId != NavigateConstants.DEFAULT_MOVIE_ID) {
            bundle.putInt(AppConstants.ARG_MOVIE_ID, movieId);
        }
        fragment.setArguments(bundle);
    }


    private String getFragmentTag(Fragment fragment) {
        return String.valueOf(fragment.hashCode());
    }

    @Override
    public void onShowError(String message) {
    }

    @Override
    public void showDialog() {
    }

    @Override
    public void hideDialog() {
    }

    public interface HomeCallbackListener extends Parcelable {
        void onNavigate(int index, int movieId);
    }

    private HomeCallbackListener homeCallbackListener = new HomeCallbackListener() {
        @Override
        public void onNavigate(int index, int movieId) {
            homePresenter.navigateToPage(index, movieId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {

        }
    };
}
