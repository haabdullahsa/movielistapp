package com.tmdbapp.thelatestmovies.view.child.detail;

import com.tmdbapp.thelatestmovies.provider.network.NetworkComponent;
import com.tmdbapp.thelatestmovies.provider.scope.FragmentScope;

import dagger.Component;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@FragmentScope
@Component(dependencies = NetworkComponent.class, modules = MovieDetailModule.class)
public interface MovieDetailComponent {
    void inject(MovieDetailFragment movieDetailFragment);
}
