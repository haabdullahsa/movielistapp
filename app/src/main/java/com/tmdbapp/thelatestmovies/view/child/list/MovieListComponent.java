package com.tmdbapp.thelatestmovies.view.child.list;

import com.tmdbapp.thelatestmovies.provider.network.NetworkComponent;
import com.tmdbapp.thelatestmovies.provider.scope.FragmentScope;

import dagger.Component;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@FragmentScope
@Component(dependencies = NetworkComponent.class, modules = MovieListModule.class)
public interface MovieListComponent {
    void inject(MovieListFragment movieListFragment);
}
