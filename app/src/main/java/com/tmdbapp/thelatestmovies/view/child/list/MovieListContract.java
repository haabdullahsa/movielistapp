package com.tmdbapp.thelatestmovies.view.child.list;

import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.view.base.BasePresenter;
import com.tmdbapp.thelatestmovies.view.base.BaseView;

import java.util.List;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public interface MovieListContract {

    interface MovieListView extends BaseView {
        void fillList(List<Movie> movieList);
    }

    interface MovieListPresenter extends BasePresenter<MovieListContract.MovieListView> {
        void getLatestMovies();

        void getLatestMoviesByDate(String releaseDate);

        void resetDatas();

        void onLoadMore(String releaseDate);
    }
}
