package com.tmdbapp.thelatestmovies.view.child.list;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.tmdbapp.thelatestmovies.R;
import com.tmdbapp.thelatestmovies.core.TheLatestMoviesApp;
import com.tmdbapp.thelatestmovies.provider.constant.AppConstants;
import com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants;
import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.view.child.list.adapter.MovieListAdapter;
import com.tmdbapp.thelatestmovies.view.parent.HomeActivity;
import com.tmdbapp.thelatestmovies.view.util.ScrollDataListListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MovieListFragment extends Fragment implements MovieListContract.MovieListView {

    HomeActivity.HomeCallbackListener homeCallbackListener;

    private String releaseDate = "";
    private boolean isScrolled = false;

    private ProgressDialog progressDialog; //TODO -- it will be updated

    @Inject
    MovieListContract.MovieListPresenter movieListPresenter;

    @BindView(R.id.recycler_movies)
    RecyclerView recyclerMovieList;

    @BindView(R.id.edittext_filter)
    EditText editTextDate;

    @OnClick(R.id.button_filter)
    void onClick() {
        if (!editTextDate.getText().toString().trim().equals(releaseDate)) {
            releaseDate = editTextDate.getText().toString();
            isScrolled = false;
            movieListPresenter.getLatestMoviesByDate(releaseDate);
        }
    }

    private MovieListAdapter movieListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);
        final Unbinder unbinder = ButterKnife.bind(this, rootView);
        initDagger();
        movieListPresenter.onAttachView(this, unbinder);
        initView();
        movieListPresenter.getLatestMovies();
        return rootView;
    }

    private void initView() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.loading_message));
        movieListAdapter = new MovieListAdapter(getContext(), movie -> goToDetailPage(movie.getId()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerMovieList.setHasFixedSize(true);
        recyclerMovieList.setLayoutManager(linearLayoutManager);
        recyclerMovieList.setAdapter(movieListAdapter);
        recyclerMovieList.addOnScrollListener(scrollDataListListener);
    }

    private void initDagger() {
        DaggerMovieListComponent.builder()
                .networkComponent(((TheLatestMoviesApp) getActivity().getApplication()).getNetworkComponent())
                .build().inject(this);
    }

    private void getExtras() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            homeCallbackListener = bundle.getParcelable(AppConstants.ARG_CALLBACK_TO_ACTIVITY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getExtras();
    }

    private void goToDetailPage(int movieId) {
        if (homeCallbackListener != null) {
            homeCallbackListener.onNavigate(NavigateConstants.DETAİL_PAGE_NUM, movieId);
        }
    }

    @Override
    public void onShowError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialog() {
        progressDialog.show();
    }

    @Override
    public void hideDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        movieListPresenter.onDetachView();
        movieListAdapter.unbindTracking();
    }

    @Override
    public void fillList(List<Movie> movieList) {
        if (isScrolled) {
            movieListAdapter.updateDatas(movieList);
        } else {
            movieListAdapter.addDatas(movieList);
        }
    }

    private ScrollDataListListener scrollDataListListener = new ScrollDataListListener() {
        @Override
        public void onLoadMore() {
            isScrolled = true;
            movieListPresenter.getLatestMoviesByDate(releaseDate);
        }
    };
}
