package com.tmdbapp.thelatestmovies.view.child.list.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.tmdbapp.thelatestmovies.R;
import com.tmdbapp.thelatestmovies.provider.constant.AppConstants;
import com.tmdbapp.thelatestmovies.provider.model.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public class MovieListAdapter extends RecyclerView.Adapter<MovieItemHolder> {

    private List<Movie> movieList;
    private Context context;
    private MovieItemHolder movieItemHolder;
    private OnItemClickListener onItemClickListener;

    public MovieListAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.context = context;
        this.movieList = new ArrayList<>();
    }

    @Override
    public MovieItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        movieItemHolder = new MovieItemHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_movie, parent, false));
        return movieItemHolder;
    }

    @Override
    public void onBindViewHolder(MovieItemHolder holder, int position) {
        holder.textTitle.setText(movieList.get(position).getOriginalTitle());
        holder.textOverview.setText(movieList.get(position).getOverview());
        Glide.with(context).load(AppConstants.IMAGE_URL + movieList.get(position).getPosterPath())
                .into(holder.imagePoster);
        click(movieList.get(position), holder.itemView);
    }

    @Override
    public int getItemCount() {
        return movieList != null ? movieList.size() : 0;
    }

    public void updateDatas(List<Movie> movieList) {
        this.movieList.addAll(movieList);
        notifyDataSetChanged();
    }

    public void addDatas(List<Movie> movieList) {
        if (!this.movieList.isEmpty()) {
            this.movieList.clear();
        }
        this.movieList.addAll(movieList);
        notifyDataSetChanged();
    }

    public void unbindTracking() {
        movieItemHolder.unBind();
    }

    private void click(Movie movie, View itemView) {
        itemView.setOnClickListener(view -> onItemClickListener.onItemClick(movie));
    }

    public interface OnItemClickListener {
        void onItemClick(Movie movie);
    }
}
