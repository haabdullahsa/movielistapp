package com.tmdbapp.thelatestmovies.view.child.list;

import com.tmdbapp.thelatestmovies.provider.interactor.MovieInteractor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abdullahsa on 15.11.2017.
 */
@Module
class MovieListModule {

    @Provides
    MovieListContract.MovieListPresenter providePresenterImpl(MovieInteractor movieInteractor) {
        return new MovieListPresenterImpl(movieInteractor);
    }

}
