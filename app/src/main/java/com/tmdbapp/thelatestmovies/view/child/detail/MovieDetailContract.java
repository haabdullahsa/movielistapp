package com.tmdbapp.thelatestmovies.view.child.detail;

import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.view.base.BasePresenter;
import com.tmdbapp.thelatestmovies.view.base.BaseView;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public interface MovieDetailContract {

    interface MovieDetailView extends BaseView {
        void showDetail(Movie movie);
    }

    interface MovieDetailPresenter extends BasePresenter<MovieDetailContract.MovieDetailView> {
        void getMovieDetail(int movieId);
    }

}
