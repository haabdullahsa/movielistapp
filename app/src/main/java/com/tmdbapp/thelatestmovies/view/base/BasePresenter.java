package com.tmdbapp.thelatestmovies.view.base;

import butterknife.Unbinder;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public interface BasePresenter<BaseViewType extends BaseView> {
    void onAttachView(BaseViewType baseView, Unbinder unbinder);

    void onDetachView();
}
