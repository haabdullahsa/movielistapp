package com.tmdbapp.thelatestmovies.view.parent;

import com.tmdbapp.thelatestmovies.view.base.BaseView;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public interface HomeContract {

    interface HomeView extends BaseView {
        void onShowMovieList();

        void onShowMovieDetail(int movieId);
    }

    interface HomePresenter {
        void navigateToPage(int index, int movieId);
    }

}
