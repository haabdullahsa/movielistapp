package com.tmdbapp.thelatestmovies.view.child.detail;

import com.tmdbapp.thelatestmovies.provider.interactor.MovieInteractor;
import com.tmdbapp.thelatestmovies.provider.manager.ErrorManager;
import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.view.base.BasePresenterImpl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public class MovieDetailPresenterImpl extends BasePresenterImpl<MovieDetailContract.MovieDetailView>
        implements MovieDetailContract.MovieDetailPresenter {

    private MovieInteractor movieInteractor;

    MovieDetailPresenterImpl(MovieInteractor movieInteractor) {
        this.movieInteractor = movieInteractor;
    }

    @Override
    public void getMovieDetail(int movieId) {
        baseView.showDialog();
        compositeDisposable.add(movieInteractor.queryMovieDetailById(movieId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ErrorManager<Movie>(baseView) {
                    @Override
                    public void onSuccess(Movie movie) {
                        baseView.hideDialog();
                        baseView.showDetail(movie);
                    }
                }));
    }
}
