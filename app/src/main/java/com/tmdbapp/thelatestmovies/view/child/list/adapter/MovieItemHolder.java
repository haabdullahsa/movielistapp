package com.tmdbapp.thelatestmovies.view.child.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tmdbapp.thelatestmovies.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public class MovieItemHolder extends RecyclerView.ViewHolder {

    private Unbinder unbinder;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.text_overview)
    TextView textOverview;

    @BindView(R.id.image_poster)
    ImageView imagePoster;

    public MovieItemHolder(View itemView) {
        super(itemView);
        unbinder = ButterKnife.bind(this, itemView);
    }

    protected void unBind() {
        unbinder.unbind();
        unbinder = null;
    }
}
