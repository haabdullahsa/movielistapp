package com.tmdbapp.thelatestmovies.view.parent;

import com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants;
import com.tmdbapp.thelatestmovies.view.base.BasePresenterImpl;

/**
 * Created by abdullahsa on 14.11.2017.
 */

public class HomePresenterImpl extends BasePresenterImpl<HomeContract.HomeView> implements HomeContract.HomePresenter {

    HomePresenterImpl() {
    }

    @Override
    public void navigateToPage(int var, int movieId) {
        if (var == NavigateConstants.MOVIE_LIST_PAGE_NUM) {
            baseView.onShowMovieList();
        } else if (movieId != NavigateConstants.DEFAULT_MOVIE_ID) {
            baseView.onShowMovieDetail(movieId);
        }
    }

}
