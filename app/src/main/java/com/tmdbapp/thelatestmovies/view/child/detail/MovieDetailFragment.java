package com.tmdbapp.thelatestmovies.view.child.detail;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tmdbapp.thelatestmovies.R;
import com.tmdbapp.thelatestmovies.core.TheLatestMoviesApp;
import com.tmdbapp.thelatestmovies.provider.constant.AppConstants;
import com.tmdbapp.thelatestmovies.provider.constant.NavigateConstants;
import com.tmdbapp.thelatestmovies.provider.model.Movie;
import com.tmdbapp.thelatestmovies.view.parent.HomeActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MovieDetailFragment extends Fragment implements MovieDetailContract.MovieDetailView {

    HomeActivity.HomeCallbackListener homeCallbackListener;

    private int movieId;
    ;

    @Inject
    MovieDetailContract.MovieDetailPresenter movieDetailPresenter;

    @BindView(R.id.image_detail_poster)
    ImageView imagePoster;

    @BindView(R.id.text_detail_title)
    TextView textTitle;

    @BindView(R.id.text_detail_overview)
    TextView textOverview;

    @BindView(R.id.text_release_date)
    TextView textReleaseDate;

    private ProgressDialog progressDialog; //TODO -- it will be updated

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        final Unbinder unbinder = ButterKnife.bind(this, rootView);
        initDagger();
        movieDetailPresenter.onAttachView(this, unbinder);
        return rootView;
    }

    private void initDagger() {
        DaggerMovieDetailComponent.builder()
                .networkComponent(((TheLatestMoviesApp) getActivity().getApplication()).getNetworkComponent())
                .build().inject(this);
    }

    private void getExtras() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            movieId = bundle.getInt(AppConstants.ARG_MOVIE_ID, NavigateConstants.DEFAULT_MOVIE_ID);
            homeCallbackListener = bundle.getParcelable(AppConstants.ARG_CALLBACK_TO_ACTIVITY);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getExtras();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.loading_message));
        if (movieId != NavigateConstants.DEFAULT_MOVIE_ID) {
            movieDetailPresenter.getMovieDetail(movieId);
        }
    }

    private void goToMovieList() {
        if (homeCallbackListener != null) {
            homeCallbackListener.onNavigate(NavigateConstants.MOVIE_LIST_PAGE_NUM, NavigateConstants.DEFAULT_MOVIE_ID);
        }
    }

    @Override
    public void onShowError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDialog() {
        progressDialog.show();
    }

    @Override
    public void hideDialog() {
        progressDialog.dismiss();
    }

    @Override
    public void showDetail(Movie movie) {
        Glide.with(this).load(AppConstants.IMAGE_URL + movie.getPosterPath()).into(imagePoster);
        textTitle.setText(movie.getOriginalTitle());
        textOverview.setText(movie.getOverview());
        textReleaseDate.setText(movie.getReleaseDate());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        movieDetailPresenter.onDetachView();
    }
}
