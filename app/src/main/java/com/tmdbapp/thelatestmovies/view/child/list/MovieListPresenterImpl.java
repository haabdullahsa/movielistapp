package com.tmdbapp.thelatestmovies.view.child.list;

import com.tmdbapp.thelatestmovies.provider.interactor.MovieInteractor;
import com.tmdbapp.thelatestmovies.provider.manager.ErrorManager;
import com.tmdbapp.thelatestmovies.provider.model.MovieListResponse;
import com.tmdbapp.thelatestmovies.view.base.BasePresenterImpl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by abdullahsa on 15.11.2017.
 */

public class MovieListPresenterImpl extends BasePresenterImpl<MovieListContract.MovieListView>
        implements MovieListContract.MovieListPresenter {

    private MovieInteractor movieInteractor;

    private int currentPage = 1;
    private boolean isLoadDataFinished = false;

    MovieListPresenterImpl(MovieInteractor movieInteractor) {
        this.movieInteractor = movieInteractor;
    }

    @Override
    public void getLatestMovies() {
        baseView.showDialog();
        compositeDisposable.add(movieInteractor.queryTheLatestMovieList("", currentPage)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ErrorManager<MovieListResponse>(baseView) {
                    @Override
                    public void onSuccess(MovieListResponse movieListResponse) {
                        baseView.hideDialog();
                        if (movieListResponse.getPage() == movieListResponse.getTotalPages()) {
                            isLoadDataFinished = true;
                        } else {
                            currentPage = movieListResponse.getPage();
                        }
                        baseView.fillList(movieListResponse.getMovies());
                    }
                }));
    }

    @Override
    public void getLatestMoviesByDate(String releaseDate) {
        baseView.showDialog();
        resetDatas();
        compositeDisposable.add(movieInteractor.queryTheLatestMovieList(releaseDate, currentPage)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new ErrorManager<MovieListResponse>(baseView) {
                    @Override
                    public void onSuccess(MovieListResponse movieListResponse) {
                        baseView.hideDialog();
                        if (movieListResponse.getPage() == movieListResponse.getTotalPages()) {
                            isLoadDataFinished = true;
                        } else {
                            currentPage = movieListResponse.getPage();
                        }
                        baseView.fillList(movieListResponse.getMovies());
                    }
                }));
    }

    @Override
    public void resetDatas() {
        isLoadDataFinished = false;
        currentPage = 1;
    }

    @Override
    public void onLoadMore(String releaseDate) {
        if (!isLoadDataFinished) {
            getLatestMoviesByDate(releaseDate);
        }
    }
}
